# hex-light-piper-3d-model

![](./media/hex-light-piper-assembly.JPG)

CAD Parametric (freecad) 

![](cad/parametrics-cad.drawio.png)

fit m5Stack LED hexagonal module
https://shop.m5stack.com/products/neo-hex-37-rgb-led-board-ws2812


![](cad/mesures-hex.drawio.png)

![](https://shop.m5stack.com/cdn/shop/products/5_f8fe3b3d-d470-4de6-8837-16b3bbb0ecd8_1200x1200.jpg?v=1652434742)
